# 学习 C++ 宏的用法
## 关键字 define

调试小技巧：
+ 使用 `g++ -E main.cpp -o main.i` 命令，
  对代码只进行预处理，从而展开宏
+ [https://cppinsights.io/](https://cppinsights.io/): 可以将代码中的宏定义展开

g++ 版本：13.1.1
